pragma solidity ^0.5.0;

import "@openzeppelin/contracts-ethereum-package/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/access/roles/CapperRole.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/crowdsale/validation/PausableCrowdsale.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/crowdsale/emission/MintedCrowdsale.sol";

/**
 * @title DaemonCreditsPresale
 * @dev Crowdsale with 30 ETH per-beneficiary cap.
 */
contract DaemonCreditsPresale is Initializable, Crowdsale, PausableCrowdsale, MintedCrowdsale, CapperRole {
    using SafeMath for uint256;

    mapping(address => uint256) private _contributions;
    mapping(address => uint256) private _caps;

    uint256 private _individualDefaultCap;

    function initialize(uint256 rate, address payable wallet, IERC20 token, uint256 individualDefaultCap) public initializer {
        _individualDefaultCap = individualDefaultCap;

        Crowdsale.initialize(rate, wallet, token);
        CapperRole.initialize(_msgSender());
        PausableCrowdsale.initialize(_msgSender());
    }

    /**
     * @dev Sets a specific beneficiary's maximum contribution.
     * @param beneficiary Address to be capped
     * @param cap Wei limit for individual contribution
     */
    function setCap(address beneficiary, uint256 cap) external onlyCapper {
        _caps[beneficiary] = cap;
    }

    /**
     * @dev Returns the cap of a specific beneficiary.
     * @param beneficiary Address whose cap is to be checked
     * @return Current cap for individual beneficiary
     */
    function getCap(address beneficiary) public view returns (uint256) {
        uint256 cap = _caps[beneficiary];
        if (cap == 0) {
            cap = _individualDefaultCap;
        }
        return cap;
    }

    /**
     * @dev Returns the amount contributed so far by a specific beneficiary.
     * @param beneficiary Address of contributor
     * @return Beneficiary contribution so far
     */
    function getContribution(address beneficiary) public view returns (uint256) {
        return _contributions[beneficiary];
    }

    /**
     * @dev Extend parent behavior requiring purchase to respect the beneficiary's funding cap.
     * @param beneficiary Token purchaser
     * @param weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal view {
        super._preValidatePurchase(beneficiary, weiAmount);
        // solhint-disable-next-line max-line-length
        require(
            _contributions[beneficiary].add(weiAmount) <= getCap(beneficiary),
            "DaemonCreditsPresale: beneficiary's cap exceeded"
        );
    }

    /**
     * @dev Extend parent behavior to update beneficiary contributions.
     * @param beneficiary Token purchaser
     * @param weiAmount Amount of wei contributed
     */
    function _updatePurchasingState(address beneficiary, uint256 weiAmount) internal {
        super._updatePurchasingState(beneficiary, weiAmount);
        _contributions[beneficiary] = _contributions[beneficiary].add(weiAmount);
    }

    uint256[50] private ______gap;
}
