const Web3 = require('web3');
const { setupLoader } = require('@openzeppelin/contract-loader');
const { BN, ether } = require('@openzeppelin/test-helpers');

async function main() {
  const rate = new BN('1000');
  const value = ether('5');
  const cap = ether('10');
  const individualDefaultCap = ether('30');

  // Our code will go here
  // Set up web3 object, connected to the local development network
  const web3 = new Web3('http://localhost:8545');

  // Retrieve accounts from the local node
  const accounts = await web3.eth.getAccounts();
  console.log(accounts);
  const wallet = accounts[1];
  const loader = setupLoader({ provider: web3 }).web3;

  // Set up a web3 contract, representing our deployed Box instance, using the contract loader
  const crowdsaleAddress = '0xC89Ce4735882C9F0f0FE26686c53074E09B0D550';
  // mainnet
  // const crowdsaleAddress = '0xCF14FC9Ead55Bf6791c63A25834f5cD8543D082A';
  const presale = loader.fromArtifact('DaemonCreditsPresale', crowdsaleAddress);

  const tokenAddress = '0xCfEB869F69431e42cdB54A4F4f105C19C080A601';
  // mainnet
  // const tokenAddress = '0xD9A3A0Fa900Eb9B0F1219143727c0ff1153E121d';
  const token = loader.fromArtifact('DaemonCredits', tokenAddress);

  // Token is initialized during deployment.
  // await token.methods.initialize().send({ from: accounts[0] });

  // Crowdsale is initialized during deployment.
  // console.log('initializing crowdsale...');
  // await presale.methods.initialize(rate, wallet, tokenAddress, individualDefaultCap).send({ from: accounts[0] })
  //   .catch(function(rej) { console.log(rej); });

  console.log('adding minter...');
  await token.methods.addMinter(crowdsaleAddress).send({ from: accounts[0] });

  // never gonna give you up.
  // console.log('renouncing minter...');
  // await token.methods.renounceMinter().send({ from: accounts[0] });

}

main();