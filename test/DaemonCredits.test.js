// Tooken from parts of: @openzeppelin/contracts-ethereum-package

const { accounts, contract } = require('@openzeppelin/test-environment');
const { BN } = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const ERC20DetailedMock = contract.fromArtifact('DaemonCreditsMock');

const { shouldBehaveLikeERC20Mintable } = require('./behaviors/ERC20Mintable.behavior');
const ERC20MintableMock = contract.fromArtifact('DaemonCreditsMock');
const { shouldBehaveLikePublicRole } = require('./behaviors/access/roles/PublicRole.behavior');

describe('DaemonCredits.ERC20Detailed', function () {
  const _name = 'DaemonCredits';
  const _symbol = 'CREDITS';
  const _decimals = new BN(18);

  beforeEach(async function () {
    this.detailedERC20 = await ERC20DetailedMock.new();
  });

  it('has a name', async function () {
    expect(await this.detailedERC20.name()).to.equal(_name);
  });

  it('has a symbol', async function () {
    expect(await this.detailedERC20.symbol()).to.equal(_symbol);
  });

  it('has an amount of decimals', async function () {
    expect(await this.detailedERC20.decimals()).to.be.bignumber.equal(_decimals);
  });
});

describe('DaemonCredits.ERC20Mintable', function () {
  const [ minter, otherMinter, ...otherAccounts ] = accounts;

  beforeEach(async function () {
    this.token = await ERC20MintableMock.new({ from: minter });
  });

  describe('minter role', function () {
    beforeEach(async function () {
      this.contract = this.token;
      await this.contract.addMinter(otherMinter, { from: minter });
    });

    shouldBehaveLikePublicRole(minter, otherMinter, otherAccounts, 'minter');
  });

  shouldBehaveLikeERC20Mintable(minter, otherAccounts);
});