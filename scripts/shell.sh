#!/usr/bin/env sh

docker run -it \
    -v $PWD:/home/user/node/workspace/ \
    --workdir=/home/user/node/workspace/ \
    node:10 \
    bash