const { balance, expectEvent } = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

function shouldBehaveLikeIndividuallyCappedMintedCrowdsale
([ deployer, investor, wallet, purchaser ], rate, cap, value) {
  const expectedTokenAmount = rate.mul(value);

  describe('as a minted crowdsale', function () {
    beforeEach(async function () {
      await this.crowdsale.setCap(investor, cap, { from: deployer });
    });

    describe('accepting payments', function () {
      it('should accept payments', async function () {
        await this.crowdsale.send(value, { from: investor });
        await this.crowdsale.buyTokens(investor, { value: value, from: purchaser });
      });
    });

    describe('high-level purchase', function () {
      it('should log purchase', async function () {
        const result = await this.crowdsale.sendTransaction({ value: value, from: investor });
        expectEvent(result, 'TokensPurchased', {
          purchaser: investor,
          beneficiary: investor,
          value: value,
          amount: expectedTokenAmount,
        });
      });

      it('should assign tokens to sender', async function () {
        await this.crowdsale.sendTransaction({ value: value, from: investor });
        expect(await this.token.balanceOf(investor)).to.be.bignumber.equal(expectedTokenAmount);
      });

      it('should forward funds to wallet', async function () {
        const balanceTracker = await balance.tracker(wallet);
        await this.crowdsale.sendTransaction({ value, from: investor });
        expect(await balanceTracker.delta()).to.be.bignumber.equal(value);
      });
    });
  });
}

module.exports = {
  shouldBehaveLikeIndividuallyCappedMintedCrowdsale,
};
