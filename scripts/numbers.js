const { BN, ether } = require('@openzeppelin/test-helpers');

async function main() {
  const rate = new BN('10000');
  const individualDefaultCap = ether('50');

  console.log("rate:", rate.toString(10));
  console.log("individualDefaultCap:", individualDefaultCap.toString(10));
}

main();