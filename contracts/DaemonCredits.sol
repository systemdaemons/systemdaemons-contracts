pragma solidity ^0.5.0;

import "@openzeppelin/contracts-ethereum-package/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Detailed.sol";

/**
 * @title DaemonCreditsPresale
 * @dev Crowdsale with 30 ETH per-beneficiary cap.
 */
contract DaemonCredits is ERC20Mintable, ERC20Detailed {
    using SafeMath for uint256;

    function initialize() public initializer {
        ERC20Detailed.initialize("DaemonCredits", "CREDITS", 18);
        ERC20Mintable.initialize(_msgSender());
    }

    uint256[50] private ______gap;
}
