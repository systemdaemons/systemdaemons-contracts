// Tooken from parts of: @openzeppelin/contracts-ethereum-package

const { accounts, contract } = require('@openzeppelin/test-environment');

const { BN, ether, expectRevert } = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const IndividuallyCappedCrowdsaleImpl = contract.fromArtifact('DaemonCreditsPresaleImpl');
const ERC20Mintable = contract.fromArtifact('DaemonCreditsMock');

const { shouldBehaveLikePublicRole } = require('./behaviors/access/roles/PublicRole.behavior');

describe('DaemonCreditsPresale.IndividuallyCappedCrowdsale', function () {
  const [ capper, otherCapper, wallet, alice, bob, charlie, other, ...otherAccounts ] = accounts;

  const rate = new BN(1);
  const capAlice = ether('10');
  const capBob = ether('2');
  const lessThanCapAlice = ether('6');
  const lessThanCapBoth = ether('1');
  const individualDefaultCap = ether('30');

  beforeEach(async function () {
    this.token = await ERC20Mintable.new({ from: capper });
    this.crowdsale = await IndividuallyCappedCrowdsaleImpl.new(rate, wallet, this.token.address, individualDefaultCap, { from: capper });

    await this.token.addMinter(this.crowdsale.address, { from: capper });
    await this.token.renounceMinter({ from: capper });
  });

  describe('capper role', function () {
    beforeEach(async function () {
      this.contract = this.crowdsale;
      await this.contract.addCapper(otherCapper, { from: capper });
    });

    shouldBehaveLikePublicRole(capper, otherCapper, otherAccounts, 'capper');
  });

  describe('individual caps', function () {
    it('sets a cap when the sender is a capper', async function () {
      await this.crowdsale.setCap(alice, capAlice, { from: capper });
      expect(await this.crowdsale.getCap(alice)).to.be.bignumber.equal(capAlice);
    });

    it('reverts when a non-capper sets a cap', async function () {
      await expectRevert(this.crowdsale.setCap(alice, capAlice, { from: other }),
        'CapperRole: caller does not have the Capper role',
      );
    });

    context('with individual caps', function () {
      beforeEach(async function () {
        await this.crowdsale.setCap(alice, capAlice, { from: capper });
        await this.crowdsale.setCap(bob, capBob, { from: capper });
      });

      describe('accepting payments', function () {
        it('should accept payments within cap', async function () {
          await this.crowdsale.buyTokens(alice, { value: lessThanCapAlice });
          await this.crowdsale.buyTokens(bob, { value: lessThanCapBoth });
        });

        it('should reject payments outside cap', async function () {
          await this.crowdsale.buyTokens(alice, { value: capAlice });
          await expectRevert(this.crowdsale.buyTokens(alice, { value: 1 }),
            'DaemonCreditsPresale: beneficiary\'s cap exceeded',
          );
        });

        it('should reject payments that exceed cap', async function () {
          await expectRevert(this.crowdsale.buyTokens(alice, { value: capAlice.addn(1) }),
            'DaemonCreditsPresale: beneficiary\'s cap exceeded',
          );
          await expectRevert(this.crowdsale.buyTokens(bob, { value: capBob.addn(1) }),
            'DaemonCreditsPresale: beneficiary\'s cap exceeded',
          );
        });

        it('should manage independent caps', async function () {
          await this.crowdsale.buyTokens(alice, { value: lessThanCapAlice });
          await expectRevert(this.crowdsale.buyTokens(bob, { value: lessThanCapAlice }),
            'DaemonCreditsPresale: beneficiary\'s cap exceeded',
          );
        });

        it('should default to the initialized cap', async function () {
          expect(await this.crowdsale.getCap(charlie)).to.be.bignumber.equal(individualDefaultCap);
          await this.crowdsale.buyTokens(charlie, { value: individualDefaultCap });
          await expectRevert(this.crowdsale.buyTokens(other, { value: individualDefaultCap + ether('1') }),
            'DaemonCreditsPresale: beneficiary\'s cap exceeded',
          );
        });
      });

      describe('reporting state', function () {
        it('should report correct cap', async function () {
          expect(await this.crowdsale.getCap(alice)).to.be.bignumber.equal(capAlice);
        });

        it('should report actual contribution', async function () {
          await this.crowdsale.buyTokens(alice, { value: lessThanCapAlice });
          expect(await this.crowdsale.getContribution(alice)).to.be.bignumber.equal(lessThanCapAlice);
        });
      });
    });
  });
});

const PausableCrowdsale = contract.fromArtifact('DaemonCreditsPresaleImpl');

describe('DaemonCreditsPresale.PausableCrowdsale', function () {
  const [ pauser, wallet, other ] = accounts;

  const rate = new BN(1);
  const value = new BN(1);
  const capOther = ether('10');
  const individualDefaultCap = ether('30');

  beforeEach(async function () {
    const from = pauser;

    this.token = await ERC20Mintable.new({ from: from });
    this.crowdsale = await PausableCrowdsale.new(rate, wallet, this.token.address, individualDefaultCap, { from: from });

    await this.token.addMinter(this.crowdsale.address, { from: pauser });
    await this.token.renounceMinter({ from: pauser });
  });

  it('purchases work', async function () {
    await this.crowdsale.sendTransaction({ from: other, value });
    await this.crowdsale.buyTokens(other, { from: other, value });
  });

  context('after pause', function () {
    beforeEach(async function () {
      await this.crowdsale.pause({ from: pauser });
    });

    it('purchases do not work', async function () {
      await expectRevert(this.crowdsale.sendTransaction({ from: other, value }),
        'Pausable: paused',
      );
      await expectRevert(this.crowdsale.buyTokens(other, { from: other, value }),
        'Pausable: paused',
      );
    });

    context('after unpause', function () {
      beforeEach(async function () {
        await this.crowdsale.unpause({ from: pauser });
      });

      it('purchases work', async function () {
        await this.crowdsale.sendTransaction({ from: other, value });
        await this.crowdsale.buyTokens(other, { from: other, value });
      });
    });
  });
});

const { shouldBehaveLikeIndividuallyCappedMintedCrowdsale } =
  require('./crowdsale/IndividuallyCappedMintedCrowdsale.behavior');

const MintedCrowdsaleImpl = contract.fromArtifact('DaemonCreditsPresaleImpl');
const ERC20 = contract.fromArtifact('ERC20');

describe('DaemonCreditsPresale.MintedCrowdsale', function () {
  const [ deployer, investor, wallet, purchaser ] = accounts;

  const rate = new BN('1000');
  const value = ether('5');
  const cap = ether('10');
  const individualDefaultCap = ether('30');

  describe('using ERC20Mintable', function () {
    beforeEach(async function () {
      this.token = await ERC20Mintable.new({ from: deployer });
      this.crowdsale = await MintedCrowdsaleImpl.new(rate, wallet, this.token.address, individualDefaultCap, { from: deployer });

      await this.token.addMinter(this.crowdsale.address, { from: deployer });
      await this.token.renounceMinter({ from: deployer });
    });

    it('crowdsale should be minter', async function () {
      expect(await this.token.isMinter(this.crowdsale.address)).to.equal(true);
    });

    shouldBehaveLikeIndividuallyCappedMintedCrowdsale([deployer, investor, wallet, purchaser], rate, cap, value);
  });

  describe('using non-mintable token', function () {
    beforeEach(async function () {
      this.token = await ERC20.new();
      this.crowdsale = await MintedCrowdsaleImpl.new(rate, wallet, this.token.address, individualDefaultCap);
    });

    it('rejects bare payments', async function () {
      await expectRevert.unspecified(this.crowdsale.send(value));
    });

    it('rejects token purchases', async function () {
      await expectRevert.unspecified(this.crowdsale.buyTokens(investor, { value: value, from: purchaser }));
    });
  });
});
