pragma solidity ^0.5.0;

import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/crowdsale/validation/PausableCrowdsale.sol";

contract PausableCrowdsaleImpl is PausableCrowdsale {
    constructor (uint256 _rate, address payable _wallet, ERC20 _token) public {
        Crowdsale.initialize(_rate, _wallet, _token);
        PausableCrowdsale.initialize(_msgSender());
    }
}
