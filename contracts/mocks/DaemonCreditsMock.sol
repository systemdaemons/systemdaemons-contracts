pragma solidity ^0.5.0;

// import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Mintable.sol";
import "../DaemonCredits.sol";
import "./MinterRoleMock.sol";

contract DaemonCreditsMock is DaemonCredits, MinterRoleMock {
    constructor() public {
        DaemonCredits.initialize();
    }
}
