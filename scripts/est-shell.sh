#!/usr/bin/env sh

docker run -it \
    -v $PWD:/opt/workspace \
    --workdir=/opt/workspace/ \
    trailofbits/eth-security-toolbox:latest
