pragma solidity ^0.5.0;

import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/crowdsale/validation/IndividuallyCappedCrowdsale.sol";
import "../DaemonCreditsPresale.sol";
import "./CapperRoleMock.sol";

contract DaemonCreditsPresaleImpl is DaemonCreditsPresale, CapperRoleMock {
    constructor (uint256 rate, address payable wallet, IERC20 token, uint256 individualDefaultCap) public {
        DaemonCreditsPresale.initialize(rate, wallet, token, individualDefaultCap);
    }
}